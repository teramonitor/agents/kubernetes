##################
#      Logs      #
##################

# Resources
resource "kubernetes_cluster_role_v1" "tm_logs" {
  count = var.tm_logs_enabled == true ? 1 : 0
  metadata {
    name = var.tm_logs_name
  }
  rule {
    verbs      = ["get", "watch", "list"]
    api_groups = ["*"]
    resources = [
      "nodes",
      "nodes/proxy",
      "services",
      "endpoints",
      "pods",
      "serviceaccount"
    ]
  }
}

resource "kubernetes_cluster_role_binding_v1" "tm_logs" {
  count = var.tm_logs_enabled == true ? 1 : 0
  metadata {
    name = var.tm_logs_name
  }
  subject {
    kind      = "ServiceAccount"
    name      = var.tm_logs_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = var.tm_logs_name
  }
}

resource "kubernetes_service_account_v1" "tm_logs" {
  count = var.tm_logs_enabled == true ? 1 : 0
  metadata {
    name      = var.tm_logs_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
}

resource "kubernetes_config_map_v1" "tm_logs_configmap" {
  count = var.tm_logs_enabled == true ? 1 : 0
  metadata {
    name      = var.tm_logs_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
  data = {
    "promtail.yaml" = var.tm_logs_custom_config ? templatefile("${path.cwd}/templates/custom_logs_config.yaml.tpl",
      {
        env                  = var.env
        cluster_name         = var.cluster_name
        tm_logs_url          = var.tm_logs_url
        tm_logs_username     = var.tm_logs_username
        tm_logs_password     = var.tm_logs_password
        tm_logs_parsing_type = var.tm_logs_parsing_type
    }) : templatefile("${path.module}/templates/logs_default_config.yaml.tpl",
      {
        env                  = var.env
        cluster_name         = var.cluster_name
        tm_logs_url          = var.tm_logs_url
        tm_logs_username     = var.tm_logs_username
        tm_logs_password     = var.tm_logs_password
        tm_logs_parsing_type = var.tm_logs_parsing_type
    }) 
  }
}

resource "kubernetes_daemon_set_v1" "tm_logs" {
  count = var.tm_logs_enabled == true ? 1 : 0
  metadata {
    name      = var.tm_logs_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name

    labels = {
      k8s-app = var.tm_logs_name
    }
  }
  spec {
    selector {
      match_labels = {
        k8s-app = var.tm_logs_name
      }
    }
    template {
      metadata {
        labels = {
          k8s-app = var.tm_logs_name
        }
      }
      spec {
        volume {
          name = "config"
          config_map {
            name = var.tm_logs_name
          }
        }
        volume {
          name = "run"
          host_path {
            path = "/run/promtail"
          }
        }
        volume {
          name = "containers"
          host_path {
            path = "/var/lib/docker/containers"
          }
        }
        volume {
          name = "pods"
          host_path {
            path = "/var/log/pods"
          }
        }
        container {
          security_context {
            privileged = true
          }
          image_pull_policy = "IfNotPresent"
          name              = var.tm_logs_name
          image             = var.tm_logs_image
          args              = ["-config.file=/etc/promtail/promtail.yaml", "-config.expand-env=true"]
          port {
            name           = "http-metrics"
            container_port = 3101
            protocol       = "TCP"
          }
          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "spec.nodeName"
              }
            }
          }
          volume_mount {
            name       = "config"
            mount_path = "/etc/promtail"
          }
          volume_mount {
            name       = "run"
            mount_path = "/run/promtail"
          }
          volume_mount {
            name       = "containers"
            read_only  = true
            mount_path = "/var/lib/docker/containers"
          }
          volume_mount {
            name       = "pods"
            read_only  = true
            mount_path = "/var/log/pods"
          }
        }
        restart_policy                   = "Always"
        dns_policy                       = "ClusterFirst"
        service_account_name             = var.tm_logs_name
        automount_service_account_token  = true
        termination_grace_period_seconds = 30
      }
    }
  }
}
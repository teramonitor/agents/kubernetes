#####################
#      Metrics      #
#####################

# Resources

resource "kubernetes_cluster_role_v1" "tm_metrics" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name = var.tm_metrics_name
  }
  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["*"]
    resources = [
      "configmaps",
      "endpoints",
      "persistentvolumeclaims",
      "persistentvolumeclaims/status",
      "pods",
      "pods/log",
      "pods/status",
      "secrets",
      "replicationcontrollers",
      "replicationcontrollers/scale",
      "replicationcontrollers/status",
      "serviceaccounts",
      "services",
      "services/status",
      "bindings",
      "events",
      "limitranges",
      "namespaces",
      "namespaces/status",
      "resourcequotas",
      "resourcequotas/status",
      "controllerrevisions",
      "daemonsets",
      "daemonsets/status",
      "deployments",
      "deployments/scale",
      "deployments/status",
      "replicasets",
      "replicasets/scale",
      "replicasets/status",
      "statefulsets",
      "statefulsets/scale",
      "statefulsets/status",
      "horizontalpodautoscalers",
      "horizontalpodautoscalers/status",
      "cronjobs",
      "cronjobs/status",
      "jobs",
      "jobs/status",
      "ingresses",
      "ingresses/status",
      "networkpolicies",
      "poddisruptionbudgets",
      "poddisruptionbudgets/status",
      "persistentvolumes",
      "nodes",
      "nodes/log",
      "nodes/metrics",
      "nodes/proxy",
      "nodes/spec",
      "nodes/stats",
      "stats/summary",
    ]
  }
}

resource "kubernetes_cluster_role_binding_v1" "tm_metrics" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name = var.tm_metrics_name
  }
  subject {
    kind      = "ServiceAccount"
    name      = var.tm_metrics_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = var.tm_metrics_name
  }
}

resource "kubernetes_service_account_v1" "tm_metrics" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name      = var.tm_metrics_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
}

resource "kubernetes_config_map_v1" "tm_metrics_ds_configmap" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name      = "${var.tm_metrics_name}-ds"
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
  data = {
    "telegraf.conf" = var.tm_metrics_custom_config ? templatefile("${path.cwd}/templates/custom_metrics_ds_config.conf.tpl",
    {
      env                  = var.env
      cluster_name         = var.cluster_name
      tm_metrics_tsdb_type = var.tm_metrics_tsdb_type
    }) : templatefile("${path.module}/templates/metrics_ds_default_config.conf.tpl",
    {
      env                  = var.env
      cluster_name         = var.cluster_name
      tm_metrics_tsdb_type = var.tm_metrics_tsdb_type
    })
  }
}

resource "kubernetes_daemon_set_v1" "tm_metrics_ds" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name      = "${var.tm_metrics_name}-ds"
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name

    labels = {
      app = "${var.tm_metrics_name}-ds"
    }
  }
  spec {
    selector {
      match_labels = {
        app = "${var.tm_metrics_name}-ds"
      }
    }
    template {
      metadata {
        labels = {
          app = "${var.tm_metrics_name}-ds"
        }
      }
      spec {
        volume {
          name = "sys"
          host_path {
            path = "/sys"
          }
        }
        volume {
          name = "proc"
          host_path {
            path = "/proc"
          }
        }
        volume {
          name = "utmp"
          host_path {
            path = "/var/run/utmp"
          }
        }
        volume {
          name = "config"
          config_map {
            name = "${var.tm_metrics_name}-ds"
          }
        }
        container {
          security_context {
            run_as_user                = 100
            allow_privilege_escalation = true
          }
          name    = "${var.tm_metrics_name}-ds"
          image   = var.tm_metrics_image
          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                field_path = "spec.nodeName"
              }
            }
          }
          env {
            name = "HOSTIP"
            value_from {
              field_ref {
                field_path = "status.hostIP"
              }
            }
          }
          env {
            name  = "HOST_PROC"
            value = "/rootfs/proc"
          }
          env {
            name  = "HOST_SYS"
            value = "/rootfs/sys"
          }
          env {
            name = "ENV"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "ENV"
              }
            }
          }
          env {
            name = "TSDB_USERNAME"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_USERNAME"
              }
            }
          }
          env {
            name = "TSDB_PASSWORD"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_PASSWORD"
              }
            }
          }
          env {
            name = "TSDB_HOST"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_HOST"
              }
            }
          }
          env {
            name = "TSDB_DATABASE"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_DATABASE"
              }
            }
          }
          env {
            name = "TSDB_TYPE"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_TYPE"
              }
            }
          }
          env {
            name  = "INPUT_INTERVAL"
            value = var.metrics_input_interval
          }
          env {
            name  = "FLUSH_INTERVAL"
            value = var.metrics_flush_interval
          }
          env {
            name  = "COLLECTION_JITTER"
            value = var.metrics_collection_jitter
          }
          env {
            name  = "FLUSH_JITTER"
            value = var.metrics_flush_jitter
          }
          env {
            name  = "KUBERNETES_INPUT_SCHEME"
            value = var.metrics_kubernetes_input_scheme
          }
          env {
            name  = "KUBERNETES_INPUT_PORT"
            value = var.metrics_kubernetes_input_port
          }
          env {
            name  = "KUBERNETES_SVC"
            value = var.metrics_kubernetes_inventory
          }
          env {
            name  = "EUID"
            value = 1
          }
          env {
            name  = "STATSD_PORT"
            value = var.tm_metrics_statsd_port
          }
          resources {
            limits = {
              memory = var.metrics_memory_limit
            }
            requests = {
              cpu    = var.metrics_cpu_requests
              memory = var.metrics_memory_requests
            }
          }
          volume_mount {
            name       = "sys"
            read_only  = true
            mount_path = "/rootfs/sys"
          }
          volume_mount {
            name       = "proc"
            read_only  = true
            mount_path = "/rootfs/proc"
          }
          volume_mount {
            name       = "utmp"
            read_only  = true
            mount_path = "/var/run/utmp"
          }
          volume_mount {
            name       = "config"
            mount_path = "/etc/telegraf"
          }
        }
        service_account_name             = var.tm_metrics_name
        automount_service_account_token  = true
        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_service" "tm_metrics" {
  count = var.tm_metrics_enabled ? 1 : 0
  metadata {
    name      = var.tm_metrics_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
    labels = {
      app = "${var.tm_metrics_name}-ds"
    }
  }
  spec {
    port {
      protocol    = "UDP"
      port        = var.tm_metrics_statsd_port
      target_port = var.tm_metrics_statsd_port
    }

    selector = {
      app = "${var.tm_metrics_name}-ds"
    }
    type = "NodePort"
  }
}

resource "kubernetes_secret_v1" "tm_metrics" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name      = var.tm_metrics_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
    labels = {
      app = var.tm_metrics_name
    }
  }
  type = "Opaque"
  data = {
    ENV           = var.env
    TSDB_HOST     = var.tm_metrics_tsdb_type == "influxdb" ? "${var.tm_metrics_tsdb_schema}://${var.tm_metrics_tsdb_host}:${var.tm_metrics_tsdb_port}" : var.tm_metrics_tsdb_host
    TSDB_DATABASE = var.tm_metrics_tsdb_dbname
    TSDB_USERNAME = var.tm_metrics_tsdb_username
    TSDB_PASSWORD = var.tm_metrics_tsdb_password
    TSDB_TYPE     = var.tm_metrics_tsdb_type
  }
}

# Resources
resource "kubernetes_config_map_v1" "tm_metrics_configmap" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name      = var.tm_metrics_name
    namespace = kubernetes_namespace_v1.teramonitor.metadata[0].name
  }
  data = {
    "telegraf.conf" = var.tm_deployment_metrics_config != "false" ? var.tm_deployment_metrics_config : templatefile("${path.module}/templates/metrics_default_config.conf.tpl",
    {
      env                  = var.env
      cluster_name         = var.cluster_name
      tm_metrics_tsdb_type = var.tm_metrics_tsdb_type
    })
  }
}

resource "kubernetes_deployment_v1" "tm_metrics" {
  count = var.tm_metrics_enabled == true ? 1 : 0
  metadata {
    name        = var.tm_metrics_name
    namespace   = kubernetes_namespace_v1.teramonitor.metadata[0].name

    labels = {
      app = var.tm_metrics_name
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.tm_metrics_name
      }
    }
    template {
      metadata {
        annotations = {
          config_change = sha1(
            jsonencode(
              merge(
                kubernetes_secret_v1.tm_metrics[0].data,
                kubernetes_config_map_v1.tm_metrics_configmap[0].data,
              )
            )
          )
        }
        labels = {
          app = var.tm_metrics_name
        }
      }
      spec {
        volume {
          name = "sys"
          host_path {
            path = "/sys"
          }
        }
        volume {
          name = "proc"
          host_path {
            path = "/proc"
          }
        }
        volume {
          name = "utmp"
          host_path {
            path = "/var/run/utmp"
          }
        }
        volume {
          name = "config"
          config_map {
            name = var.tm_metrics_name
          }
        }
        container {
          security_context {
            run_as_user                = 100
            allow_privilege_escalation = true
          }
          name    = var.tm_metrics_name
          image   = var.tm_metrics_image
          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                field_path = "spec.nodeName"
              }
            }
          }
          env {
            name = "HOSTIP"
            value_from {
              field_ref {
                field_path = "status.hostIP"
              }
            }
          }
          env {
            name  = "HOST_PROC"
            value = "/rootfs/proc"
          }
          env {
            name  = "HOST_SYS"
            value = "/rootfs/sys"
          }
          env {
            name = "ENV"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "ENV"
              }
            }
          }
          env {
            name = "TSDB_USERNAME"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_USERNAME"
              }
            }
          }
          env {
            name = "TSDB_PASSWORD"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_PASSWORD"
              }
            }
          }
          env {
            name = "TSDB_HOST"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_HOST"
              }
            }
          }
          env {
            name = "TSDB_DATABASE"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_DATABASE"
              }
            }
          }
          env {
            name = "TSDB_TYPE"
            value_from {
              secret_key_ref {
                name = var.tm_metrics_name
                key  = "TSDB_TYPE"
              }
            }
          }
          env {
            name  = "INPUT_INTERVAL"
            value = var.metrics_input_interval
          }
          env {
            name  = "FLUSH_INTERVAL"
            value = var.metrics_flush_interval
          }
          env {
            name  = "COLLECTION_JITTER"
            value = var.metrics_collection_jitter
          }
          env {
            name  = "FLUSH_JITTER"
            value = var.metrics_flush_jitter
          }
          env {
            name  = "KUBERNETES_INPUT_SCHEME"
            value = var.metrics_kubernetes_input_scheme
          }
          env {
            name  = "KUBERNETES_INPUT_PORT"
            value = var.metrics_kubernetes_input_port
          }
          env {
            name  = "KUBERNETES_SVC"
            value = var.metrics_kubernetes_inventory
          }
          env {
            name  = "EUID"
            value = 1
          }
          env {
            name  = "STATSD_PORT"
            value = var.tm_metrics_statsd_port
          }
          resources {
            limits = {
              memory = var.metrics_memory_limit
            }
            requests = {
              cpu    = var.metrics_cpu_requests
              memory = var.metrics_memory_requests
            }
          }
          volume_mount {
            name       = "sys"
            read_only  = true
            mount_path = "/rootfs/sys"
          }
          volume_mount {
            name       = "proc"
            read_only  = true
            mount_path = "/rootfs/proc"
          }
          volume_mount {
            name       = "utmp"
            read_only  = true
            mount_path = "/var/run/utmp"
          }
          volume_mount {
            name       = "config"
            mount_path = "/etc/telegraf"
          }
        }
        service_account_name             = var.tm_metrics_name
        automount_service_account_token  = true
        termination_grace_period_seconds = 30
      }
    }
  }
}
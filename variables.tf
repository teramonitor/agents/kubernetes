######################
#   MAIN Variables   #
######################

# Client

variable "client_name" {
  description = "Client Name"
  type        = string
  default     = ""
}

variable "env" {
  description = "Environment name"
  type        = string
  default     = ""
}

variable "cluster_name" {
  description = "Cluster Name"
  type        = string
  default     = ""
}

# Metrics
variable "tm_metrics_enabled" {
  description = "Is teramonitor metrics enabled?"
  type        = bool
  default     = false
}

variable "tm_metrics_name" {
  description = "Teramonitor metrics name"
  type        = string
  default     = "tm-metrics"
}

variable "tm_metrics_image" {
  description = "Teramonitor metrics image"
  type        = string
  default     = "telegraf:1.27.2-alpine"
}

variable "tm_metrics_custom_config" {
  description = "Teramonitor metrics custom configuration?"
  type        = bool
  default     = false
}

variable "tm_deployment_metrics_config" {
  description = "Teramonitor deployment metrics custom configuration?"
  type        = string
  default     = "false"
}

variable "tm_metrics_ds_custom_config" {
  description = "Teramonitor metrics daemonset custom configuration?"
  type        = bool
  default     = false
}

variable "tm_metrics_config" {
  description = "Teramonitor metrics configuration"
  default     = null
}

variable "tm_metrics_statsd_port" {
  description = "Teramonitor metrics StatsD port"
  default     = 8125
}

variable "tm_metrics_tsdb_type" {
  description = "TSDB Type (Only supported for now influxdb)"
  type        = string
  default     = "influxdb"
}

variable "tm_metrics_tsdb_schema" {
  description = "TSDB Schema"
  type        = string
  default     = "https"
}

variable "tm_metrics_tsdb_host" {
  description = "TSDB Host"
  type        = string
  default     = ""
}

variable "tm_metrics_tsdb_port" {
  description = "TSDB Port"
  type        = number
  default     = 8086
}

variable "tm_metrics_tsdb_dbname" {
  description = "TSDB Database Name"
  type        = string
  default     = ""
}

variable "tm_metrics_tsdb_username" {
  description = "TSDB Username"
  type        = string
  default     = ""
}

variable "tm_metrics_tsdb_password" {
  description = "TSDB Password"
  type        = string
  default     = ""
}

variable "metrics_input_interval" {
  description = "TM metrics input interval"
  type        = string
  default     = "30s"
}

variable "metrics_flush_interval" {
  description = "TM metrics flush interval"
  type        = string
  default     = "30s"
}

variable "metrics_collection_jitter" {
  description = "TM metrics collection jitter"
  type        = string
  default     = "0s"
}

variable "metrics_flush_jitter" {
  description = "TM metrics flush jitter"
  type        = string
  default     = "0s"
}

variable "metrics_kubernetes_input_scheme" {
  description = "TM metrics kubernetes input scheme"
  type        = string
  default     = "https"
}

variable "metrics_kubernetes_input_port" {
  description = "TM metrics kubernetes input port"
  type        = string
  default     = "10250"
}

variable "metrics_kubernetes_inventory" {
  description = "TM metrics kubernetes inventory"
  type        = string
  default     = "kubernetes.default.svc"
}

variable "metrics_memory_limit" {
  description = "TM metrics memory limit"
  type        = string
  default     = "256Mi"
}

variable "metrics_cpu_requests" {
  description = "TM metrics cpu requests"
  type        = string
  default     = "100m"
}

variable "metrics_memory_requests" {
  description = "TM metrics memory requests"
  type        = string
  default     = "256Mi"
}

# Logs
variable "tm_logs_enabled" {
  description = "Is teramonitor logs enabled?"
  type        = bool
  default     = false
}

variable "tm_logs_name" {
  description = "Teramonitor logs name"
  type        = string
  default     = "tm-logs"
}

variable "tm_logs_image" {
  description = "Teramonitor logs image"
  type        = string
  default     = "docker.io/grafana/promtail:2.5.0"
}

variable "tm_logs_custom_config" {
  description = "Teramonitor logs custom configuration?"
  type        = bool
  default     = false
}

variable "tm_logs_url" {
  description = "Logs Password"
  type        = string
  default     = ""
}

variable "tm_logs_username" {
  description = "Logs Password"
  type        = string
  default     = ""
}

variable "tm_logs_password" {
  description = "Logs Password"
  type        = string
  default     = ""
}

variable "tm_logs_parsing_type" {
  description = "Logs parsing type"
  type        = string
  default     = "docker: {}"
}
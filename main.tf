######################
#       MAIN         #
######################

# Variables 
variable "teramonitor_name" {
  default = "teramonitor"
}

# Resources 
resource "kubernetes_namespace_v1" "teramonitor" {
  metadata {
    name = var.teramonitor_name
  }
}